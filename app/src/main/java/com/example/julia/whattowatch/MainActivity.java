package com.example.julia.whattowatch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Movie> movieList = new ArrayList<>();
        for (int i=0; i<10; i++) {
            movieList.add(i, new Movie("Movie " + i));
        }

        GridView layout = (GridView) findViewById(R.id.grid);
        layout.setAdapter(new MovieGridAdapter(this, movieList));

    }
}

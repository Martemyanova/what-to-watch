package com.example.julia.whattowatch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MovieGridAdapter extends BaseAdapter {

    private Context context;
    private List<Movie> movieList;

    public MovieGridAdapter(Context context, List<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public Object getItem(int i) {
        return movieList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        Movie movie = movieList.get(i);
        holder.title.setText(movie.title);
        Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(holder.image);

        return view;
    }

    public static class ViewHolder {

        private TextView title;
        private ImageView image;

        public ViewHolder(View root) {
            title = (TextView) root.findViewById(R.id.movie_item_title);
            image = (ImageView) root.findViewById(R.id.movie_item_image);
        }
    }
}
